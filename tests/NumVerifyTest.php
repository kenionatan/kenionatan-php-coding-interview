<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

class NumVerifyTest extends TestCase {

    private $baseUrl = 'http://apilayer.net/api/validate';
    private $accessKey = '7dcdbaa5eb9314da2b31ff4d3426921b';
    private $phoneNumber = '14158586273';
    private $httpClient;

    public function setUp(): void
    {
        parent::setUp();
        $this->httpClient = new Client();
    }

	public function testApiRequest()
    {
        $url = $this->baseUrl . "?access_key=" . $this->accessKey . "&number=" . $this->phoneNumber . "&country_code=&format=1";

        $response = $this->httpClient->get($url);

        $this->assertSame(200, $response->getStatusCode());        

        $body = $response->getBody()->getContents();

        // print response
        $prettyPrintedResponse = json_encode(json_decode($body), JSON_PRETTY_PRINT);
        echo $prettyPrintedResponse;

        $this->assertNotEmpty($body);
        $decodedResponse = json_decode($body);
        $this->assertNotNull($decodedResponse);
    }
}