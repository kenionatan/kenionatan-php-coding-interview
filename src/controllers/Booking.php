<?php

namespace Src\controllers;

use Src\models\BookingModel;

class Booking {

	protected function getBookingModel(): BookingModel {
		return new BookingModel();
	}

	public function getBookings() {
		return $this->getBookingModel()->getBookings();
	}

	public function addBooking($booking) {
		return $this->getBookingModel()->addBooking($booking);
	}
}